import Prelude hiding (String)

x = show (read "123" :: Int)

badSum :: Num p => [p] -> p
badSum [] = 0
badSum (x : xs) = x + sum xs

showSum :: (Num a, Show a) => [a] -> [Char]
showSum xs = show (sum xs)

type String = [Char]
type Point = (Double, Double)
midpoint :: Point -> Point -> Point
midpoint (x1,y1) (x2,y2) =
  ((x1 + x2) / 2, (y1 + y2) / 2)


newtype CustomerId = MakeCustomerId Int
badCustomer :: CustomerId
badCustomer = MakeCustomerId 13

customerToInt :: CustomerId -> Int
customerToInt (MakeCustomerId i) = i

-- Records
-- data Customer = MakeCustomer 
--   { customerId  :: CustomerId
--   , name        :: String
--   , luckyNumber :: Int
--   }

-- alice :: Customer
-- alice = MakeCustomer
--   { customerId = MakeCustomerId 1
--   , name = "Alice"
--   , luckyNumber = 12
--   }

-- sally = alice { name = "Sally", luckyNumber = 84 }

-- Algebraic Data Types
data Customer = Customer CustomerId String Int
alice :: Customer
alice = Customer (MakeCustomerId 13) "Alice" 42

getCustomerId :: Customer -> CustomerId
-- getCustomerId (Customer cust_id name luckyNumber) = cust_id
getCustomerId (Customer cust_id _ _) = cust_id

data StringTree = StringTree String [StringTree]
hierarchy = StringTree "C:"
              [ StringTree "Program Files" []
              , StringTree "Users"
                  [StringTree "Alice" []]
              , StringTree "Cats" []
              ]

data MaybeInt = NoInt | JustInt Int
defaultInt :: Int -> MaybeInt -> Int
defaultInt defaultValue NoInt = defaultValue
defaultInt _ (JustInt x) = x

data StringList = EmptyStringList | ConsStringList String StringList

data Maybe' a = MyJust a | MyNothing
fromMaybe :: a -> Maybe' a -> a
fromMaybe defaultVal MyNothing = defaultVal
fromMaybe _ (MyJust x) = x

data MyListImpl a = Empty | Cons a (MyListImpl a)