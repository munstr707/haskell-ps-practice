compose f g x = f (g x)
add1 x = x + 1
mult2 x = 2 * x
plus1 = (+) 1
plus x y = (+) x y
double = map (2*)

notNull xs = not (null xs)

isEven x = x `mod` 2 == 0
removeOdd = filter isEven

showPlus s x = "(" ++ s ++ "+" ++ (show x) ++ ")"
showAllAdditions nums = foldl showPlus "0" nums

-- function operators
-- . composes functions
stringLength :: Int -> Int
stringLength = length . show

notNull' :: Foldable t => t a -> Bool
notNull' = not . null

-- $ passes args to functions