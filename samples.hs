square x = x * x
-- find the max and then multiply by x
multMax a b x = (max a b) * x

posOrNeg x = if x >= 0 then "Positive" else "Negative"

power2 n = 
  if n == 0
    then 1
  else 2 * (power2 (n-1))

repeatString str n = 
  if n == 0 
    then ""
  else str ++ (repeatString str (n-1))

-- lists
list1 = [1,2,3]
-- add to beginning
list2 = 0 : list1
-- add to end
list3 = list2 ++ [4,5]
-- head on list returns firt ele
-- tail on list returns all but head
-- null tests whether a list is empty

double nums =
    if null nums
      then []
    else (2 * (head nums)) : (double (tail nums))

-- recursion really not needed
removeOdd nums =
  if null nums 
    then []
  else
    if (mod (head nums) 2) == 0
      then (head nums) : (removeOdd (tail nums))
    else removeOdd (tail nums)

-- tuples
testTup = (1, "test")

-- pattern matching
fst' (a, b) = a
scnd' (a, b) = b

-- bad as it crashes everything
head' (x : xs) = x
head' [] = error "head of empty list"

-- pattern matching in the function definition
dblPatternMatching [] = []
dblPatternMatching (x : xs) = (2 * x) : (dblPatternMatching xs)

-- GUARDS
pow2Pattern n 
  | n == 0 = 1
  | otherwise = 2 * (pow2Pattern (n-1))

removeOdds2 [] = []
removeOdds2 (x : xs)
  | mod x 2 == 0 = x : (removeOdds2 xs)
  | otherwise = removeOdds2 xs

-- CASE EXPRESSIONS
doubleCase nums = case nums of 
  [] -> []
  (x : xs) -> (2 * x) : (double xs)

anyEvens nums = case (removeOdds2 nums) of 
  [] -> False
  (x : xs) -> True

-- let bindings
fancy7 = 
    let a = 3 
    in 2 * a + 1

fancy9 =
    let x = 4
        y = 5
    in x + y

numEven nums =
  let evenNums = removeOdds2 nums
  in length evenNums

fancyWhere7 = 2 * a + 1 where a = 3
fancyWhere9 = x + y 
  where x = 4
        y = 5