myElem :: Eq a => a -> [a] -> Bool
myElem _ [] = False
myElem x (y : ys)
  | x == y = True
  | otherwise = myElem x ys

data RGB = RGB Int Int Int
instance Eq RGB where
  (RGB r1 g1 b1) == (RGB r2 g2 b2) =
    (r1 == r2) && (g1 == g2) && (b1 == b2) 

colors = [RGB 255 0 0, RGB 0 255 0, RGB 0 0 255]
green = RGB 0 255 0
greenInColors = elem green colors
